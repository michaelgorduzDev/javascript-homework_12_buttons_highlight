const buttons = document.querySelectorAll(".btn");
let setActive = null;

document.addEventListener("keypress", (e) => {
  const key = e.key.toLowerCase();

  buttons.forEach((button) => {
    if (button.textContent.toLowerCase() === key) {
      function checkColor() {
        if (button.style.backgroundColor !== "blue") {
          button.style.backgroundColor = "blue";
          setActive = button;
          buttons.forEach((element) => {
            if (element !== setActive) {
              element.style.backgroundColor = "black";
            }
          });
        } else {
          button.style.backgroundColor = "black";
          setActive = null;
        }
      }
      
      checkColor();
      button.click(); // Simulate a click on the matching button
    }
  });
});

